// 'use strict';
// import * as Hapi from '@hapi/hapi';

// class TodoItem {
//     task: string;
//     status: boolean;

//     constructor(task:string,status = false){
//         this.task =task;
//         this.status = status;
//     }
// }


// const init = async () => {
//     const connections  ={
//         url: 'mongodb://localhost:27017/mychanneldb',        
//         settings: {
//             poolSize: 10
//         },
//         decorate: true
//     }

//     const server = new Hapi.Server({ port: 3000, host: '0.0.0.0' });



//     await server.register([
//         // require('./todos'),
//         require('./todos-routes'),
//         {
//             plugin: require('hapi-mongodb'),
//             options: connections
//         },
//     ]);

//     await server.start();
//     console.log('Server running on %s', server.info.uri);
// };

// process.on('unhandledRejection', (err) => {
//     console.log(err);
//     process.exit(1);
// });

// init();

import * as Glue from "glue";
const connections = {
    url: 'mongodb://localhost:27017/mychanneldb',
    settings: {
        poolSize: 10
    },
    decorate: true
};

const manifest = {
    server: {
        host: '0.0.0.0',
        port: '3000'
    },
    register: {
        plugins: [
            {
                plugin: require('hapi-mongodb'),
                options: connections
            },
            {
                plugin: require('./todos')
            }, {
                plugin: require('./todos-routes'),
            }
        ]
    }
};

const options: any = {
    relativeTo: __dirname
};
const startServer = async () => {

    try {
        const server = await Glue.compose(manifest, options);
        await server.start()
        console.log('Server running on %s', server.info.uri);
    } catch (error) {
        console.log(error);
        process.exit(1);
    }

}
startServer();
