
export default class Plugin {
    name: string = 'todosPlugin';
    version: string = '1.0.0';
    todos = new Array();
    register = async function (server, options) {

        server.method({
            name: 'todos.list',
            method: async () => {
                const db = server.mongo.db;
                return await db.collection('todoAuey').find().toArray();
            }
        });

        server.method({
            name: 'todos.add',
            method: async () => {
                const db = server.mongo.db;
                return await db.collection('todoAuey').insert(
                    {
                        task: "Task 6", status: true 
                    }
                )
            }
        });

        server.method({
            name: 'todos.patch',
            // method: (item) => {
            //     console.log(item);
            //     for (let i = 0; i < this.todos.length; i++) {
            //         if (this.todos[i].id == Number(item)) {
            //             console.log(this.todos[i]);
            //             if (this.todos[i].done == false) {
            //                 console.log(this.todos[i].done);
            //                 this.todos[i].done = true
            //             } else {
            //                 this.todos[i].done = false
            //             }
            //         }
            //     }
            //     return this.todos;
            // }
            method : ()=>{
                const db = server.mongo.db;
                return db.collection('todoAuey').updateOne(
                    {task: 'Task 5' },
                    {
                        $set:{status:false}
                    }
                )

            }
        });

        // server.method({
        //     name: 'todos.delete',
        //     method: (item) => {
        //         for (var i = 0; i < this.todos.length; i++) {
        //             if (this.todos[i].id == Number(item)) {
        //                 console.log("delete", this.todos[i]);
        //                 this.todos.splice(i, 1)
        //                 console.log(this.todos.splice(i, 1));

        //             }
        //             return this.todos;
        //         }
        //     }

        // });

        server.method({
            name: 'todos.delete',
            method: (item) => {
                const db = server.mongo.db;
                return db.collection('todoAuey').deleteOne({ task : "Task 6"})
            }

        });

    }
}