"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Plugin {
    constructor() {
        this.name = 'todosPlugin';
        this.version = '1.0.0';
        this.todos = new Array();
        this.register = function (server, options) {
            return __awaiter(this, void 0, void 0, function* () {
                server.method({
                    name: 'todos.list',
                    method: () => __awaiter(this, void 0, void 0, function* () {
                        const db = server.mongo.db;
                        return yield db.collection('todoAuey').find().toArray();
                    })
                });
                server.method({
                    name: 'todos.add',
                    method: () => __awaiter(this, void 0, void 0, function* () {
                        const db = server.mongo.db;
                        return yield db.collection('todoAuey').insert({
                            task: "Task 6", status: true
                        });
                    })
                });
                server.method({
                    name: 'todos.patch',
                    // method: (item) => {
                    //     console.log(item);
                    //     for (let i = 0; i < this.todos.length; i++) {
                    //         if (this.todos[i].id == Number(item)) {
                    //             console.log(this.todos[i]);
                    //             if (this.todos[i].done == false) {
                    //                 console.log(this.todos[i].done);
                    //                 this.todos[i].done = true
                    //             } else {
                    //                 this.todos[i].done = false
                    //             }
                    //         }
                    //     }
                    //     return this.todos;
                    // }
                    method: () => {
                        const db = server.mongo.db;
                        return db.collection('todoAuey').updateOne({ task: 'Task 5' }, {
                            $set: { status: false }
                        });
                    }
                });
                // server.method({
                //     name: 'todos.delete',
                //     method: (item) => {
                //         for (var i = 0; i < this.todos.length; i++) {
                //             if (this.todos[i].id == Number(item)) {
                //                 console.log("delete", this.todos[i]);
                //                 this.todos.splice(i, 1)
                //                 console.log(this.todos.splice(i, 1));
                //             }
                //             return this.todos;
                //         }
                //     }
                // });
                server.method({
                    name: 'todos.delete',
                    method: (item) => {
                        const db = server.mongo.db;
                        return db.collection('todoAuey').deleteOne({ task: "Task 6" });
                    }
                });
            });
        };
    }
}
exports.default = Plugin;
//# sourceMappingURL=todos.js.map