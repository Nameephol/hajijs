"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hapi = require("@hapi/hapi");
const Joi = require("@hapi/joi");
class Plugin {
    constructor() {
        this.name = 'todosRoutesPlugin';
        this.version = '1.0.0';
        this.todos = new Array();
        this.register = function (server, options) {
            return __awaiter(this, void 0, void 0, function* () {
                server.route({
                    method: 'GET',
                    path: '/',
                    handler: (request, h) => {
                        return 'Hello World!';
                    },
                    options: {
                        cache: {
                            expiresIn: 30 * 1000
                        }
                    }
                });
                server.route({
                    method: "POST",
                    path: "/sum",
                    handler: (request, h) => {
                        let result = 0;
                        try {
                            result = Number(request.payload.param1) + Number(request.payload.param2);
                        }
                        catch (error) {
                            result = 0;
                        }
                        return result;
                    }
                });
                server.route({
                    method: "GET",
                    path: "/sum",
                    handler: (request, h) => {
                        let result = 0;
                        try {
                            result = request.query.param1 + request.query.param2;
                        }
                        catch (error) {
                            result = 0;
                        }
                        return result + ' ====> ' + request.query.param3;
                    },
                    options: {
                        validate: {
                            query: {
                                param1: Joi.number().integer().min(1).max(100).default(10),
                                param2: Joi.number().integer().min(1).max(100).default(10),
                                param3: Joi.boolean()
                            }
                        }
                    }
                });
                server.route({
                    method: "GET",
                    path: "/todos",
                    handler: (request, h) => {
                        return server.methods.todos.list();
                    }
                });
                server.route({
                    method: "POST",
                    path: "/todos",
                    handler: (request, h) => {
                        return server.methods.todos.add();
                    }
                });
                server.route({
                    method: "PATCH",
                    path: "/todos/{id}",
                    handler: (request, h) => {
                        return server.methods.todos.patch(request.params.id);
                    }
                });
                server.route({
                    method: "DELETE",
                    path: "/todos/{id}",
                    handler: (request, h) => {
                        console.log(request.params.id);
                        return server.methods.todos.delete(request.params.id);
                    }
                });
                server.route({
                    method: "GET",
                    path: "/issue",
                    handler: (request, reply) => __awaiter(this, void 0, void 0, function* () {
                        // const db = request.mongo.db;
                        const db = server.mongo.db;
                        try {
                            const result = yield db.collection('todos').find().toArray();
                            return result;
                        }
                        catch (err) {
                            throw Hapi.Boom.internal('Internal MongoDB error', err);
                        }
                    })
                });
            });
        };
    }
}
exports.default = Plugin;
//# sourceMappingURL=todos.routes.js.map