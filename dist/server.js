"use strict";
// 'use strict';
// import * as Hapi from '@hapi/hapi';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// class TodoItem {
//     task: string;
//     status: boolean;
//     constructor(task:string,status = false){
//         this.task =task;
//         this.status = status;
//     }
// }
// const init = async () => {
//     const connections  ={
//         url: 'mongodb://localhost:27017/mychanneldb',        
//         settings: {
//             poolSize: 10
//         },
//         decorate: true
//     }
//     const server = new Hapi.Server({ port: 3000, host: '0.0.0.0' });
//     await server.register([
//         // require('./todos'),
//         require('./todos-routes'),
//         {
//             plugin: require('hapi-mongodb'),
//             options: connections
//         },
//     ]);
//     await server.start();
//     console.log('Server running on %s', server.info.uri);
// };
// process.on('unhandledRejection', (err) => {
//     console.log(err);
//     process.exit(1);
// });
// init();
const Glue = require("glue");
const connections = {
    url: 'mongodb://localhost:27017/mychanneldb',
    settings: {
        poolSize: 10
    },
    decorate: true
};
const manifest = {
    server: {
        host: '0.0.0.0',
        port: '3000'
    },
    register: {
        plugins: [
            {
                plugin: require('hapi-mongodb'),
                options: connections
            },
            {
                plugin: require('./todos')
            }, {
                plugin: require('./todos-routes'),
            }
        ]
    }
};
const options = {
    relativeTo: __dirname
};
const startServer = () => __awaiter(this, void 0, void 0, function* () {
    try {
        const server = yield Glue.compose(manifest, options);
        yield server.start();
        console.log('Server running on %s', server.info.uri);
    }
    catch (error) {
        console.log(error);
        process.exit(1);
    }
});
startServer();
//# sourceMappingURL=server.js.map