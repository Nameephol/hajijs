import * as Hapi from '@hapi/hapi';
import * as Joi from '@hapi/joi';

export default class Plugin {
    name: string = 'todosRoutesPlugin';
    version: string = '1.0.0';
    todos = new Array();
    register = async function (server, options) {

        server.route({
            method: 'GET',
            path:'/',
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                return 'Hello World!';
            }, 
            options: {
                cache: {
                    expiresIn: 30 * 1000
                }
            }
        });
    
        server.route({
            method: "POST",
            path: "/sum",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                let result: number = 0;
                try {
                    result = Number(request.payload.param1) + Number(request.payload.param2);
                } catch (error) {
                    result = 0;
                }
                return result;
            }
        });
    
        server.route({
            method: "GET",
            path: "/sum",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                let result: number = 0;
                try {
                    result = request.query.param1 + request.query.param2;
                } catch (error) {
                    result = 0;
                }
                return result + ' ====> ' + request.query.param3;
            },
            options: {
                validate: {
                    query: {
                        param1: Joi.number().integer().min(1).max(100).default(10),
                        param2: Joi.number().integer().min(1).max(100).default(10),
                        param3: Joi.boolean()
                    }
                }
            }
        });
    
        server.route({
            method: "GET",
            path: "/todos",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
               return server.methods.todos.list()
            }   
        });
    
        server.route({
            method: "POST",
            path: "/todos",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                return server.methods.todos.add()
            }
        });
    
        server.route({
            method: "PATCH",
            path: "/todos/{id}",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                return server.methods.todos.patch(request.params.id)
            }
        });
    
        server.route({
            method: "DELETE",
            path: "/todos/{id}",
            handler: (request: Hapi.Request, h: Hapi.ResponseToolkit) => {
                console.log(request.params.id);
                return server.methods.todos.delete(request.params.id)
            }
        });
    
        server.route({
            method: "GET",
            path: "/issue",
            handler: async (request: Hapi.Request, reply: Hapi.ResponseToolkit) => {
                // const db = request.mongo.db;
        
                const db = server.mongo.db;
                try {
                    const result = await db.collection('todos').find().toArray();
                    return result;
                } catch (err) {
                    throw Hapi.Boom.internal('Internal MongoDB error', err);
                }
            }
        });
    
    }
}
